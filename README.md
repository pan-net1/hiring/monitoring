# hiring-helm-monitoring

## required binaries
* helm v3
* kubectl
* minikube or kind (tested with kind)

## install kind

### Install kind

#### Linux 

```
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.9.0/kind-linux-amd64
chmod 755 kind
```
#### macOS

```bash
brew install kind
```

### Create Cluster
`kind create cluster`

By default, kind KUBECONFIG will be in $HOME/.kube/config and should be loaded

To check if all is working type `kubectl get all`



This is expected output:
```
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   80m
```

## install chart
* clone this project
* update values file, below is working values file
```
prometheus:
  configuration: |
    scrape_configs:
      - job_name: prometheus
        static_configs:
          - targets:
            - localhost:9090
      - job_name: node-exporter
        static_configs:
          - targets:
            - node-exporter:9100
grafana:
  adminPassword: "some_pass"
```

install chart with helm
`helm upgrade --install  test ./monitoring -f monitoring/values.yaml`

By now all should be set and only thing needed is to configure grafana

## Configure grafana
port forward grafana port to be able to login
`kubectl port-forward service/grafana 3000:3000`

### Add new data source to grafana
[here](https://grafana.com/docs/grafana/latest/datasources/add-a-data-source/) is how to.
Correct values are same as prometheus service `prometheus:9090`, click **save and test**

### Import node exporter dashboard
[here](https://grafana.com/docs/grafana/latest/dashboards/export-import/#importing-a-dashboard) is how to.
Node exporter dashboard ID `11074`


Now you should be able to see data in dashboard.
